﻿### Round 3
### Aim
 To design a Deterministic Finite Automata(DFA) for given example.

### Theory
 <h3>Principle:</h3>
                   In the theory of computation, a branch of theoretical computer science, a deterministic finite automaton (DFA)—also known as deterministic finite acceptor (DFA), deterministic finite state machine (DFSM), or deterministic finite state automaton (DFSA)—is a finite-state machine that accepts or rejects a given string of symbols, by running through a state sequence uniquely determined by the string.<br><br>
                    A DFA is defined as an abstract mathematical concept, but is often implemented in hardware and software for solving various specific problems. For example, a DFA can model software that decides whether or not online user input such as email addresses are valid.<br><br>
                    <h3>Formal definition:</h3>
                    A DFA can be represented by a 5-tuple (Q, ∑, δ, q0, F) where −<br>
                    Q is a finite set of states.<br>
                    ∑ is a finite set of symbols called the alphabet.<br>
                    δ is the transition function where δ: Q × ∑ → Q<br>
                    q0 is the initial state from where any input is processed (q0 ∈ Q).<br>
                    F is a set of final state/states of Q (F ⊆ Q)<br>
                    <h3>Graphical Representation of a DFA:</h3>
                    A DFA is represented by digraphs called state diagram.<br>
                    The vertices represent the states.
                    The arcs labeled with an input alphabet show the transitions.
                    The initial state is denoted by an empty single incoming arc.
                    The final state is indicated by double circles.
                    Example
                    Let a deterministic finite automaton be →<br>
                      Q = {a, b, c},<br>
                      ∑ = {0, 1},<br>
                      q0 = {a},<br>
                      F = {c}, and<br>
                    Transition function δ as shown by the following table −<br>
                    | Present State  | Next State for Input 0  | Next State for Input 1  |
                    |---|---|---|
                    |   a |  a | b  |  
                    |   b |  c | a  |  
                    |   c |  b |  c |<br>
                    Its graphical representation would be as follows −<br><br>
                    <h4>Closure Properties:</h4><br>
                        If DFAs recognize the languages that are obtained by applying an operation on the DFA recognizable languages then DFAs are said to be closed under the operation. The DFAs are closed under the following operations.<br>

1.Union<br>
2.Intersection<br>
3.Concatenation<br>
4.Negation<br>
5.Kleene closure<br>
6.Reversal<br>
7.Init<br>
8.Quotient<br>
9.Substitution<br>
10.Homomorphism<br>
For each operation, an optimal construction with respect to the number of states has been determined in the state complexity research. Since DFAs are equivalent to nondeterministic finite automata (NFA), these closures may also be proved using closure properties of NFA.<br>
                        <h5>Steps of simulator :</h5><br>
                        1.&nbsp;The user selects the DFA from the example. <br>
                        2.&nbsp;Then user inputs the string. <br>
                        3.&nbsp;Then user presses the proceed button. <br>
                        4.&nbsp;Now final state is to be selected. <br>
                        5.&nbsp;Now user needs to press the proceed button again to check whether the string is accepted or not . <br>
                        6.&nbsp;To check for another string or DFA press reset button.     
<img src="images/dfa_graphical_representation.png"/>              

### Pre Test
1. Given the language L = {ab, aa, baa}, which of the following strings are in L*?<br>
1) abaabaaabaa<br>
2) aaaabaaaa<br>
3) baaaaabaaaab<br>
4) baaaaabaa <br>
                        <br>
                        A.<input type="radio" name="but" id="rb11" onclick="click1();">&nbsp;1,2 and 3<br>
                        B.<input type="radio" name="but" id="rb12" onclick="click1();">&nbsp;2,3 and 4
                        <br>
                        C.<input type="radio" name="but" id="rb13" onclick="click1();">&nbsp;1,2 and 4
                        <br>
                        D.<input type="radio" name="but" id="rb14" onclick="click1();">&nbsp;1,3 and 4
                        <br>
                        <p id = "p1"></p>
                        <br>
                        2.Given the language L = {ab, aa, baa}, which of the following strings are in L*?<br>
1) abaabaaabaa<br>
2) aaaabaaaa<br>
3) baaaaabaaaab<br>
4) baaaaabaa <br>
                        <br>
                       A.<input type="radio" name="but" id="rb11" onclick="click1();">&nbsp;1,2 and 3<br>
                        B.<input type="radio" name="but" id="rb12" onclick="click1();">&nbsp;2,3 and 4
                        <br>
                        C.<input type="radio" name="but" id="rb13" onclick="click1();">&nbsp;1,2 and 4
                        <br>
                        D.<input type="radio" name="but" id="rb14" onclick="click1();">&nbsp;1,3 and 4
                        <br>
                        <p id = "p2"></p>
                        <br>
                        3. Definition of a language L with alphabet {a} is given as following.<br>
             L={gate2011Q42| k>0, and n is a positive integer constant}<br>
What is the minimum number of states needed in DFA to recognize L?
                        <br>
                        A. <input type="radio" name="but3" id="rb31" onclick="click3();">&nbsp;k+1
                        <br>
                        B. <input type="radio" name="but3" id="rb32" onclick="click3();">&nbsp;n+1
                        <br>
                        C. <input type="radio" name="but3" id="rb33" onclick="click3();">&nbsp;2^(k+1)
                        <br>
                        D. <input type="radio" name="but3" id="rb34" onclick="click3();">&nbsp;2^(n+1)
                        <br><br>
                        <p id = "p3"></p>
                        <br>
                        4. Let w be any string of length n is {0,1}*. Let L be the set of all substrings of w. What is the minimum number of states in a non-deterministic finite automaton that accepts L?
                        <br>
                        A. <input type="radio" name="but4" id="rb41" onclick="click4();">&nbsp;n-1
                        <br>
                        B. <input type="radio" name="but4" id="rb42" onclick="click4();">&nbsp;n
                        <br>
                        C. <input type="radio" name="but4" id="rb43" onclick="click4();">&nbsp;n+1
                        <br>
                        D. <input type="radio" name="but4" id="rb44" onclick="click4();">&nbsp;2n-1
                        <br><br>
                        <p id = "p4"></p>
                        <br>
                        5. Which one of the following is FALSE?
                        <br>
                        A. <input type="radio" name="but5" id="rb51" onclick="click5();">&nbsp;There is unique minimal DFA for every regular language
                        <br>
                        B. <input type="radio" name="but5" id="rb52" onclick="click5();">&nbsp;Every NFA can be converted to an equivalent PDA
                        <br>
                        C. <input type="radio" name="but5" id="rb53" onclick="click5();">&nbsp;Complement of every context-free language is recursive.
                        <br>
                        D. <input type="radio" name="but5" id="rb54" onclick="click5();">&nbsp;Every nondeterministic PDA can be converted to an equivalent deterministic PDA.
                        <br><br>
                        <p id = "p5"></p>
                        <br>

### Post Test
<p style="font-size:100%; margin-top:2%">
                        1.Draw a DFA for the language accepting strings starting with ‘a’ over input alphabets ∑ = {a, b}?
                        <br>
                        2. Draw a DFA for the language accepting strings starting with ‘101’ over input alphabets ∑ = {0, 1}?
                        <br>
                        3. Draw a DFA that accepts a language L over input alphabets ∑ = {0, 1} such that L is the set of all strings starting with ’00’?
                        <br>
                        4. Construct a DFA that accepts a language L over input alphabets ∑ = {a, b} such that L is the set of all strings starting with ‘aa’ or ‘bb’?
                        <br>
                        5.Construct a DFA that accepts a language L over input alphabets ∑ = {a, b} such that L is the set of all strings starting with ‘aba’?
                    <!--Post Test of experiment -->
                    </p>


### References
<p style="font-size:100%; margin-top:2%">
                        1.&nbsp;Hopcroft, John E.; Motwani, Rajeev; Ullman, Jeffrey D. (2001). Introduction to Automata Theory, Languages, and Computation (2 ed.). Addison Wesley. ISBN 0-201-44124-1. Retrieved 19 November 2012.
                        <br><br>
                        2.&nbsp;Lawson, Mark V. (2004). Finite automata. Chapman and Hall/CRC. ISBN 1-58488-255-7. Zbl 1086.68074.
                        <br><br>
                        3.&nbsp;McCulloch, W. S.; Pitts, W. (1943). "A Logical Calculus of the Ideas Immanent in Nervous Activity" (PDF). Bulletin of Mathematical Biophysics. 5 (4): 115–133. doi:10.1007/BF02478259.
                        <br><br>
                        4.&nbsp;Sakarovitch, Jacques (2009). Elements of automata theory. Translated from the French by Reuben Thomas. Cambridge: Cambridge University Press. ISBN 978-0-521-84425-3. Zbl 1188.68177.
                        <br><br>
                        5.&nbsp;Sipser, Michael (1997). Introduction to the Theory of Computation. Boston: PWS. ISBN 0-534-94728-X.. Section 1.1: Finite Automata, pp. 31–47. Subsection "Decidable Problems Concerning Regular Languages" of section 4.1: Decidable Languages, pp. 152–155.4.4 DFA can accept only regular language
                        <br><br>
                        <h3>Webliography :</h3>
                        <br>
                        1.&nbsp;https://www.javatpoint.com/deterministic-finite-automata
                        <br>
                        2.&nbsp;https://en.wikipedia.org/wiki/Deterministic_finite_automaton
                        <br>
                        3.&nbsp;tutorialspoint.com/automata_theory/deterministic_finite_automaton.htm
                        <br><br>
                        <h3>Additional video links :</h3>
                        <br>
                        1.&nbsp;https://www.youtube.com/watch?v=zNTmVDY0kiA
                        <br>
                        2.&nbsp;https://www.youtube.com/watch?v=ubpCB5XwIeM
                  <!--Theory of experiment -->
                    </p>
