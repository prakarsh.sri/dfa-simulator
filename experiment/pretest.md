### Pre Test
1. Given the language L = {ab, aa, baa}, which of the following strings are in L*?<br>
1) abaabaaabaa<br>
2) aaaabaaaa<br>
3) baaaaabaaaab<br>
4) baaaaabaa <br>
                        <br>
                        A.<input type="radio" name="but" id="rb11" onclick="click1();">&nbsp;1,2 and 3<br>
                        B.<input type="radio" name="but" id="rb12" onclick="click1();">&nbsp;2,3 and 4
                        <br>
                        C.<input type="radio" name="but" id="rb13" onclick="click1();">&nbsp;1,2 and 4
                        <br>
                        D.<input type="radio" name="but" id="rb14" onclick="click1();">&nbsp;1,3 and 4
                        <br>
                        <p id = "p1"></p>
                        <br>
                        2.Given the language L = {ab, aa, baa}, which of the following strings are in L*?<br>
1) abaabaaabaa<br>
2) aaaabaaaa<br>
3) baaaaabaaaab<br>
4) baaaaabaa <br>
                        <br>
                       A.<input type="radio" name="but" id="rb11" onclick="click1();">&nbsp;1,2 and 3<br>
                        B.<input type="radio" name="but" id="rb12" onclick="click1();">&nbsp;2,3 and 4
                        <br>
                        C.<input type="radio" name="but" id="rb13" onclick="click1();">&nbsp;1,2 and 4
                        <br>
                        D.<input type="radio" name="but" id="rb14" onclick="click1();">&nbsp;1,3 and 4
                        <br>
                        <p id = "p2"></p>
                        <br>
                        3. Definition of a language L with alphabet {a} is given as following.<br>
             L={gate2011Q42| k>0, and n is a positive integer constant}<br>
What is the minimum number of states needed in DFA to recognize L?
                        <br>
                        A. <input type="radio" name="but3" id="rb31" onclick="click3();">&nbsp;k+1
                        <br>
                        B. <input type="radio" name="but3" id="rb32" onclick="click3();">&nbsp;n+1
                        <br>
                        C. <input type="radio" name="but3" id="rb33" onclick="click3();">&nbsp;2^(k+1)
                        <br>
                        D. <input type="radio" name="but3" id="rb34" onclick="click3();">&nbsp;2^(n+1)
                        <br><br>
                        <p id = "p3"></p>
                        <br>
                        4. Let w be any string of length n is {0,1}*. Let L be the set of all substrings of w. What is the minimum number of states in a non-deterministic finite automaton that accepts L?
                        <br>
                        A. <input type="radio" name="but4" id="rb41" onclick="click4();">&nbsp;n-1
                        <br>
                        B. <input type="radio" name="but4" id="rb42" onclick="click4();">&nbsp;n
                        <br>
                        C. <input type="radio" name="but4" id="rb43" onclick="click4();">&nbsp;n+1
                        <br>
                        D. <input type="radio" name="but4" id="rb44" onclick="click4();">&nbsp;2n-1
                        <br><br>
                        <p id = "p4"></p>
                        <br>
                        5. Which one of the following is FALSE?
                        <br>
                        A. <input type="radio" name="but5" id="rb51" onclick="click5();">&nbsp;There is unique minimal DFA for every regular language
                        <br>
                        B. <input type="radio" name="but5" id="rb52" onclick="click5();">&nbsp;Every NFA can be converted to an equivalent PDA
                        <br>
                        C. <input type="radio" name="but5" id="rb53" onclick="click5();">&nbsp;Complement of every context-free language is recursive.
                        <br>
                        D. <input type="radio" name="but5" id="rb54" onclick="click5();">&nbsp;Every nondeterministic PDA can be converted to an equivalent deterministic PDA.
                        <br><br>
                        <p id = "p5"></p>
                        <br>
