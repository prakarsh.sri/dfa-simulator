### Post Test
<p style="font-size:100%; margin-top:2%">
                        1.Draw a DFA for the language accepting strings starting with ‘a’ over input alphabets ∑ = {a, b}?
                        <br>
                        2. Draw a DFA for the language accepting strings starting with ‘101’ over input alphabets ∑ = {0, 1}?
                        <br>
                        3. Draw a DFA that accepts a language L over input alphabets ∑ = {0, 1} such that L is the set of all strings starting with ’00’?
                        <br>
                        4. Construct a DFA that accepts a language L over input alphabets ∑ = {a, b} such that L is the set of all strings starting with ‘aa’ or ‘bb’?
                        <br>
                        5.Construct a DFA that accepts a language L over input alphabets ∑ = {a, b} such that L is the set of all strings starting with ‘aba’?
                    <!--Post Test of experiment -->
                    </p>