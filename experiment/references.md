### References
<p style="font-size:100%; margin-top:2%">
                        1.&nbsp;Hopcroft, John E.; Motwani, Rajeev; Ullman, Jeffrey D. (2001). Introduction to Automata Theory, Languages, and Computation (2 ed.). Addison Wesley. ISBN 0-201-44124-1. Retrieved 19 November 2012.
                        <br><br>
                        2.&nbsp;Lawson, Mark V. (2004). Finite automata. Chapman and Hall/CRC. ISBN 1-58488-255-7. Zbl 1086.68074.
                        <br><br>
                        3.&nbsp;McCulloch, W. S.; Pitts, W. (1943). "A Logical Calculus of the Ideas Immanent in Nervous Activity" (PDF). Bulletin of Mathematical Biophysics. 5 (4): 115–133. doi:10.1007/BF02478259.
                        <br><br>
                        4.&nbsp;Sakarovitch, Jacques (2009). Elements of automata theory. Translated from the French by Reuben Thomas. Cambridge: Cambridge University Press. ISBN 978-0-521-84425-3. Zbl 1188.68177.
                        <br><br>
                        5.&nbsp;Sipser, Michael (1997). Introduction to the Theory of Computation. Boston: PWS. ISBN 0-534-94728-X.. Section 1.1: Finite Automata, pp. 31–47. Subsection "Decidable Problems Concerning Regular Languages" of section 4.1: Decidable Languages, pp. 152–155.4.4 DFA can accept only regular language
                        <br><br>
                        <h3>Webliography :</h3>
                        <br>
                        1.&nbsp;https://www.javatpoint.com/deterministic-finite-automata
                        <br>
                        2.&nbsp;https://en.wikipedia.org/wiki/Deterministic_finite_automaton
                        <br>
                        3.&nbsp;tutorialspoint.com/automata_theory/deterministic_finite_automaton.htm
                        <br><br>
                        <h3>Additional video links :</h3>
                        <br>
                        1.&nbsp;https://www.youtube.com/watch?v=zNTmVDY0kiA
                        <br>
                        2.&nbsp;https://www.youtube.com/watch?v=ubpCB5XwIeM
                  <!--Theory of experiment -->
                    </p>