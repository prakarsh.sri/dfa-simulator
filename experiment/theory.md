### Theory
 <h3>Principle:</h3>
                   In the theory of computation, a branch of theoretical computer science, a deterministic finite automaton (DFA)—also known as deterministic finite acceptor (DFA), deterministic finite state machine (DFSM), or deterministic finite state automaton (DFSA)—is a finite-state machine that accepts or rejects a given string of symbols, by running through a state sequence uniquely determined by the string.<br><br>
                    A DFA is defined as an abstract mathematical concept, but is often implemented in hardware and software for solving various specific problems. For example, a DFA can model software that decides whether or not online user input such as email addresses are valid.<br><br>
                    <h3>Formal definition:</h3>
                    A DFA can be represented by a 5-tuple (Q, ∑, δ, q0, F) where −<br>
                    Q is a finite set of states.<br>
                    ∑ is a finite set of symbols called the alphabet.<br>
                    δ is the transition function where δ: Q × ∑ → Q<br>
                    q0 is the initial state from where any input is processed (q0 ∈ Q).<br>
                    F is a set of final state/states of Q (F ⊆ Q)<br>
                    <h3>Graphical Representation of a DFA:</h3>
                    A DFA is represented by digraphs called state diagram.<br>
                    The vertices represent the states.
                    The arcs labeled with an input alphabet show the transitions.
                    The initial state is denoted by an empty single incoming arc.
                    The final state is indicated by double circles.
                    Example
                    Let a deterministic finite automaton be →<br>
                      Q = {a, b, c},<br>
                      ∑ = {0, 1},<br>
                      q0 = {a},<br>
                      F = {c}, and<br>
                    Transition function δ as shown by the following table −<br>
                    | Present State  | Next State for Input 0  | Next State for Input 1  |
                    |---|---|---|
                    |   a |  a | b  |  
                    |   b |  c | a  |  
                    |   c |  b |  c |<br>
                    Its graphical representation would be as follows −<br><br>
                    <img src="dfa_graphical_representation.jpg"
                    style="float: left; margin-centre: 1000px;" /><br><br><br><br><br><br><br><br><br><br>
                    <h4>Closure Properties:</h4><br>
                        If DFAs recognize the languages that are obtained by applying an operation on the DFA recognizable languages then DFAs are said to be closed under the operation. The DFAs are closed under the following operations.<br>

1.Union<br>
2.Intersection<br>
3.Concatenation<br>
4.Negation<br>
5.Kleene closure<br>
6.Reversal<br>
7.Init<br>
8.Quotient<br>
9.Substitution<br>
10.Homomorphism<br>
For each operation, an optimal construction with respect to the number of states has been determined in the state complexity research. Since DFAs are equivalent to nondeterministic finite automata (NFA), these closures may also be proved using closure properties of NFA.<br>
                        <h5>Steps of simulator :</h5><br>
                        1.&nbsp;The user selects the DFA from the example. <br>
                        2.&nbsp;Then user inputs the string. <br>
                        3.&nbsp;Then user presses the proceed button. <br>
                        4.&nbsp;Now final state is to be selected. <br>
                        5.&nbsp;Now user needs to press the proceed button again to check whether the string is accepted or not . <br>
                        6.&nbsp;To check for another string or DFA press reset button.     