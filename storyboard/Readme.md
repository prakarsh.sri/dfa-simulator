## Storyboard(Round 2)
 
Experiment 1: Finite Automata
 
### 1. Story Outline:
 
<div align="justify"> The experiment involves the formation of Finite Automata. In the automata theory, a branch of theoretical computer science, a deterministic finite automaton (DFA)—also known as deterministic finite state machine—is a finite state machine that accepts/rejects finite strings of symbols and only produces a unique computation (or run) of the automaton for each input string. 'Deterministic' refers to the uniqueness of the computation.
 
A DFA has a start state (denoted graphically by an arrow coming in from nowhere) where computations begin, and a set of accept states (denoted graphically by a double circle) which help define when a computation is successful. 
 
A DFA is defined as an abstract mathematical concept, but due to the deterministic nature of a DFA, it is implementable in hardware and software for solving various specific problems. For example, a DFA can model a software that decides whether or not online user-input such as email addresses are valid.
 
DFAs recognize exactly the set of regular languages which are, among other things, useful for doing lexical analysis and pattern matching. DFAs can be built from nondeterministic finite automata through the powerset construction.
 
 
 
### 2. Story:
 
#### 2.1 Visual Description:
<h2>Construction of the set-up</h2>
 
For better visualization, a simulator is provided. When we study DFA theoretically it is difficult for us to understand that how a string is validating. In this simulator we have tried to visualize this using a string and DFA . You can examine each step that how an input is given to a state and how pointer points to the next state when input is processed.
When the whole string is processed a final state is achieved.
 
#### 2.2 Set User Objectives & Goals:
Sr. No |	Learning Objective	| Cognitive Level | Action Verb
:--|:--|:--|:-:
1.| User will be able to: <br>recall the concepts of Finite Automata <br> and identify the implementation of a string using Deterministic Finite Automata. | Recall | Identify
2.| User will be able to: <br>check involvement of students ability to read course content, understand and interpret important information of DFA .<br> | Comprehension| Describe
3.| User will be able to: <br>To analyze the processing of string<br> observed on GIVEN EXAMPLE. | Analyze| Examine
 
</b>
 
#### 2.3 ADVANTAGES AND DISADVANTAGES OF DETERMINISTIC FINITE AUTOMATA
<br> <br>
<dd>• DFAs were invented to model of a Turing machine, which was too general to study properties of real world machines.
• DFAs are one of the most practical models of computation, since there is a trivial linear
time, constant-space, online algorithm there are efficient algorithms to find a DFA recognizing:
1. the complement of the language
2. the union/intersection of the languages recognized by two given DFAs.

• Because DFAs can be reduced to an efficient algorithms to determine:
1. whether a DFA accepts any strings
2. whether a DFA accepts all strings
3. whether two DFAs recognize the same language
4. the DFA with a minimum number of states for a particular regular language

• On the other hand, finite state automata are of strictly limited power in the languages they
can recognize; many simple languages, including any problem that requires more than
constant space to solve, cannot be recognized by a DFA.

• The classical example of a simply described language that no DFA can recognize is
bracket language, i.e., language that consists of properly paired brackets such as word "((
)( ))".

• No DFA can recognize the bracket language because there is no limit to recursion, i.e.,
one can always embed another pair of brackets inside.

• It would require an infinite amount of states to recognize. Another simpler example is the
language consisting of strings of the form anbn—some finite number of a's, followed by
an equal number of b's. 
 
<br>
</dd>
 
 
##### 2.4 Set Challenges and Questions/Complexity/Variations in Questions:
 
Assessment Questions:<br>
 
<dd><b>1.According to the 5-tuple representation i.e. FA= {Q, ?, d, q, F}
Statement 1: q ? Q’; Statement 2: F?Q:-<br>
   a) Statement 1 is true, Statement 2 is false.<br>
   b) Statement 1 is false, Statement 2 is true<br>
   c) Statement 1 is false, Statement 2 may be true<br>
   d) Statement 1 may be true, Statement 2 is false<br></dd><br></b>
 
<dd><b>2. dˆ tells us the best:<br>
a) how the DFA S behaves on a word u
<br>
b) the state is the dumping state
<br>
c) the final state has been reached
<br>
d) Kleene operation is performed on the set
<br><br></b>
</dd>


 
 
##### 2.5 Allow pitfalls:
<dd>i.	When a string entered is not appropriate i.e not following the condition to be implemented .<br>
ii.	If the final state is not selected what will happen .<br>
iii.	MCQ questions have to be answered at the last to check how much you have understood.<br>
</dd>
 
 
##### 2.6 Conclusion:
<dd>CONCLUSIONS:
With the help of given procedure and information about the Finite Automata 
we can easily test a given string that wheather it is Deterministic Finite Automata or not. 
</dd>
 

 
 
### 3. Flowchart
<img src="flowchart/flowchart.png"/>

 
### 4. Mindmap
<img src="mindmap/mindmap.png"/>
	

 
### 5. Storyboard 
<img src="storyboard/storyboard.gif"/>


