## Round 1
<p align="center">

<b> Design a Deterministic Finite Automata For The Given Examples </b> <a name="top"></a> <br>
</p>

<b>Discipline | </b> Computer Science
:--|:--|
<b> Lab</b> | Automata Lab
<b> Experiment</b>|1. Design a Deterministic Finite Automata


<h4> [1. Focus Area](#LO)
<h4> [2. Learning Objectives ](#LO)
<h4> [3. Instructional Strategy](#IS)
<h4> [4. Task & Assessment Questions](#AQ)
<h4> [5. Simulator Interactions](#SI)
<hr>

<a name="LO"></a>
#### 1. Focus Area : Laboratory Model of Deterministic Finite Automata
#### 2. Learning Objectives and Cognitive Level


Sr. No |	Learning Objective	| Cognitive Level | Action Verb
:--|:--|:--|:-:
1.| User will be able to: <br>Visualization of the problem using the laboratory model involves other senses <br> in the process of perception and learning, such as touch.  | Recall | Identify
2.| User will be able to: <br>See the sequence of DFA work steps on the device model.  | Understand| Describe
3.| User will be able to: <br>Apply the operations management of  the DFA model. <br> i.e.  it will be possible to make changes to the automaton settings and then observing the action of  DFA model. | Apply | Implement
4.| User will be able to: <br>To analyze the principle of operation of individual mechanical parts of the machine. <br>  | Analyze| Examine




<br/>
<div align="right">
    <b><a href="#top">↥ back to top</a></b>
</div>
<br/>
<hr>

<a name="IS"></a>
#### 3. Instructional Strategy
###### Name of Instructional Strategy  :     <u> Expository and problem based.</u>
###### Assessment Method: <u>Formative Assessment and Summative Assessment</u>

<u> <b>Description: </b> In this experiment you will learn about change in state and its relevance in real world: </u>
<br>
 <div align="justify">•	The main objective to develop this lab is to provide an interactive source of learning for the students. The simulation that we provide fulfills our purpose.
<br>
•	The learner will be easily able to understand processes in DFA .<br>
•	The user will able to understand change of state.<br>
•	With the help of our virtual lab, students get a chance to visualize for the given string how to move from starting state to final state.
<br/>
<div align="right">
    <b><a href="#top">↥ back to top</a></b>
</div>
<br/>
<hr>
<img src="images/dfa_graphical_representation.png"/>

<a name="AQ"></a>
#### 4. Task & Assessment Questions:

Read the theory and comprehend the concepts related to the experiment.
<br>

Sr. No |	Learning Objective	| Task to be performed by <br> the student  in the simulator | Assessment Questions as per LO & Task
:--|:--|:--|:-:
1.| Recall | Visualization of the problem using the laboratory model involves other senses | <div align="justify"> Which of the following statements is not true?<br> i. Every language defined by any of the automata is also defined by a regular expression<br> ii. Every language defined by a regular expression can be represented using a DFA<br> iii. For both<br> iv. None of these
2.| Understand | To see the sequence of DFA work steps on the device model. | <div align="justify">Generate a regular expression for the following problem statement:P(x): String of length 6 or less for a={0,1}<br> i. (1+0+e)6<br> ii.(10)6<br> iii. (1+0)(1+0)(1+0)(1+0)(1+0)(1+0)<br> iv. None of these
3.| Apply | Apply the operations management of  the DFA model. |<div align="justify"> The minimum number of states required in a DFA (along with a dumping state) to check whether the 3rd bit is 1 or not?
4.| Analyze | To analyze the principle of operation of individual mechanical parts of the machine. | Which one of the following is FALSE?<br> i. There is unique minimal DFA for every regular language<br> ii.Every NFA can be converted to an equivalent PDA<br> iii. Complement of every context-free language is recursive.<br> iv. Every nondeterministic PDA can be converted to an equivalent deterministic PDA.
 <br>

 <br/>
<div align="right">
    <b><a href="#top">↥ back to top</a></b>
</div>
<br/>
<hr>

<a name="SI"></a>

#### 4. Simulator Interactions:
<br>

What Students will do? |	What Simulator will do?	| Purpose of the task
:--|:--|:-:
Examine the simulator screen and take note of all the instructions. | Display all the simulator contents.  |<div align = "justify">  Display simulator interface.
Click on reset button  | Input the string in Test/Debug  |<div align = "justify"> Check the string.
Select example | DropDown menu for example  |<div align = "justify"> To know the string type.
Enter the proper time and relativestic string | Get the results respectively |<div align = "justify"> To know whether the string is accepted or not.
Answer the questions that will appear on your journey.  | Automatically putup few pitfalls.  |<div align = "justify"> To make the concept of DFA.