## Round 0

<b>Discipline | <b>Computer Sciences
:--|:--|
<b>Lab</b> | <b>Basics Of Automata and Formal Language</b>
<b>Experiment</b>| <b>1. Designing Of Deterministic Finite Automata</b>

<h5> About the Experiment : </h5>
In this experiment the user will be able to understand the basics and desinging of Deterministic Finite Automata by using an interactive simulator.

<b>Name of Developer | <b> Prachi Gupta
:--|:--|
Institute | Pranveer Singh Institute Of Technology, Kanpur, Uttar Pradesh
Email id| prachigupta.gupta52@gmail.com
Department | Information Technology(Assistant Professor)


#### Contributors List

SrNo | Name | Faculty or Student | Department| Institute | Email id
:--|:--|:--|:--|:--|:--|
1 | Prachi Gupta | Faculty | Information Technology(Assistant Professor) | Pranveer Singh Institute Of Technology, Kanpur, Uttar Pradesh | prachigupta.gupta52@gmail.com
2 | Prakarsh Srivastava | Student | Dept. of Information Technology | Pranveer Singh Institute Of Technology, Kanpur, Uttar Pradesh | prakarsh.sri@gmail.com
3 | Tanushree Mishra | Student | Dept. of Information Technology | Pranveer Singh Institute Of Technology, Kanpur, Uttar Pradesh | mishratanu270@gmail.com
4 | Kaif Abdullah | Student | Dept. of Information Technology | Pranveer Singh Institute Of Technology, Kanpur, Uttar Pradesh | kaifabdullahm@gmail.com
5 | Ishit Gera | Student | Dept. of Information Technology | Pranveer Singh Institute Of Technology, Kanpur, Uttar Pradesh | Ishitgera@gmail.com

